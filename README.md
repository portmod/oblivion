# Oblivion

This is the [Portmod](https://gitlab.com/portmod/portmod) package repository for The Elder Scrolls IV: Oblivion.

This requires that you install on top of an existing oblivion installation. See the details about installing on top of an existing directory in the [Portmod Setup Guide](https://portmod.gitlab.io/portmod/setup.html#creating-a-prefix).

It also requres the use of a custom version of bsatool: https://gitlab.com/bmwinger/bsatool2. Currently this must be compiled manually and placed in your path.
On linux, a pre-built version will be pulled in as a dependency.
